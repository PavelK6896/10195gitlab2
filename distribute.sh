#!/usr/bin/env bash
echo "$RANDOM"

echo "=================================="
date_now=$(date '+%Y-%m-%d %H:%M:%S')
text_new="readme info ${date_now}"

file=$(curl -s --header "PRIVATE-TOKEN: $DISTRIBUTOR_TOKEN" "https://gitlab.com/api/v4/projects/32428884/repository/files/README.md?ref=main")
content=$(jq -n "$file" | jq -r '.content' )
prev=$(echo -ne "$content" | base64 --decode -di)
content_new=$(echo -e "${prev} <br/>${text_new}" )

PAYLOAD=$(cat <<- JSON
{
  "branch": "main",
  "commit_message": "$text_new",
  "actions": [
    {
      "action": "update",
      "file_path": "README.md",
      "content": "$content_new"
    }
  ]
}
JSON
)
echo "$PAYLOAD"
COMMIT_INFO1=$(curl --request POST --header "PRIVATE-TOKEN: $DISTRIBUTOR_TOKEN" --header "Content-Type: application/json" \
     --data "$PAYLOAD" "https://gitlab.com/api/v4/projects/32428884/repository/commits")
echo "-----------------"
echo "$COMMIT_INFO1"
echo "================================================"


